# README

This repository is for holding a 'cuckoo community' (https://github.com/cuckoosandbox/community) style repository for extra cuckoo functionality we create to be used with alsvc_cuckoo.

While the primary goal is supporting extra features inside of AssemblyLine cuckoo service, you should be able to make use of this as an additional community repository with any
Cuckoo installation:

```
wget https://bitbucket.org/cse-assemblyline/al_cuckoo_community/get/master.tar.gz && cuckoo community --file master.tar.gz
```

# FEATURES

## dll_multi.py - Execute multiple exports

This is located at analyzer/windows/modules/packages. It's a slightly modifed version of the upstream 'dll.py'
package that is able to launch multiple dll exports in a single run by passing the export names to execute using the 
`function` option, separated by pipe character. ie/ `function=export1|export2`

